let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"]
console.log(users);

function addUsers(user) {
    users[users.length] = user
}

addUsers("John Cena");
console.log(users);

function getItem(index) {
    return users[index]
}

let itemFound = getItem(2);
console.log(itemFound);
itemFound = getItem(4);
console.log(itemFound);

function deleteLastItem() {
    const lastItem = users[users.length - 1];
    users.pop();
    return lastItem
}

let deletedItem = deleteLastItem();
console.log(users);

function updateAnItem(update, index) {
    users[index] = update;
}

updateAnItem("Triple H", 3)
console.log(users);

function deleteAllItems() {
    users = [];
}

deleteAllItems();
console.log(users);

let arrayIsEmpty = users.length === 0;
console.log(arrayIsEmpty);